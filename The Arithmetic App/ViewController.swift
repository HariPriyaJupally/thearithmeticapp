//
//  ViewController.swift
//  The Arithmetic App
//
//  Created by Jupally,Hari Priya on 2/14/19.
//  Copyright © 2019 Jupally,Hari Priya. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var pickerLabel: UIPickerView!
    
    
    @IBOutlet weak var weightTF: UITextField!
    
    
    @IBOutlet weak var exerciseTF: UITextField!
    
    
    @IBOutlet weak var caloriesTF: UILabel!
    
    
    @IBOutlet weak var minutesTF: UILabel!
    
    var pickerContents: [String] = [String]()
    
    @IBAction func clearBT(_ sender: Any) {
        weightTF.text! = ""
        pickerLabel.reloadAllComponents()
        exerciseTF.text! = ""
        caloriesTF.text! = "0 cal"
        minutesTF.text! = "0 minutes"
        pickerLabel.selectRow(0, inComponent: 0, animated: true)
    }
    
    func energyConsumed(during: String,weight: Double,time: Double) -> Double  {
        var activity = 0.0
        switch during{
        case "Bicycling", "Tennis": activity = 8.0
        case "Jumping rope": activity = 12.3
        case "Running - slow": activity = 9.8
        case "Running - fast": activity = 23.0
        case "Swimming": activity = 5.8
        default: activity = 0.0
        }
        let weightInKg = Double(weight)/2.2
        return activity * 3.5 * weightInKg/Double(200) * time
    }
    
    func timeToLose1pound(during: String,weight: Double) -> Double {
        let energyConsumedInCal = energyConsumed(during: during, weight: weight, time: 1.0)
        if weight == 0.0 {
            return 0
        }
        return Double(3500)/energyConsumedInCal
    }
    
    @IBAction func calculateBT(_ sender: Any) {
        let value = pickerContents[pickerLabel.selectedRow(inComponent : 0)]
        let defaultValue = 0.0
        let energyConsumed = self.energyConsumed(during: value, weight: Double(weightTF.text!) ?? defaultValue, time: Double(exerciseTF.text!) ?? defaultValue)
        caloriesTF.text = String(format: "%.1f cal", energyConsumed)
        let timeToLose1Pound = timeToLose1pound(during: value, weight: Double(weightTF.text!) ?? defaultValue)
        minutesTF.text! = String(format: "%.1f minutes", timeToLose1Pound)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = #colorLiteral(red: 1, green: 0.8205476403, blue: 0.9772850871, alpha: 1)
        
        self.pickerLabel.delegate = self
        self.pickerLabel.dataSource = self
        
        
        pickerContents = ["Bicycling", "Jumping rope", "Running - slow", "Running - fast", "Tennis", "Swimming"]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerContents.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerContents[row]
    }
    
    
}

